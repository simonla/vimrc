syntax on

set belloff=all " Mute annoying ring sound
set nocompatible " Do not start in vi compatible mode
set hidden " Use hidden buffers
set tabstop=4 softtabstop=4
set shiftwidth=4
set scrolloff=8
set expandtab
set smartindent
set nu
set nowrap
set ignorecase " These two lines enable case-sensitive search when a capital letter is contained, else not
set smartcase
set noswapfile
set nobackup
set undodir=%userprofile%/.vim/undodir
set undofile
set incsearch
set splitright
set encoding=utf-8
set termguicolors
set gfn=Droid_Sans_Mono_Dotted_for_Powe:h11:cANSI:qDRAFT
set backspace=indent,eol,start
set colorcolumn=125
set signcolumn=yes
set lazyredraw " redraw only when we need to
set wildmenu

highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')
Plug 'gruvbox-community/gruvbox'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
Plug 'powerline/fonts'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'lervag/vimtex'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'dpelle/vim-LanguageTool'
Plug 'easymotion/vim-easymotion'
call plug#end()

" The most convenient leader key I can imagine
let mapleader = ' '

" That's important. Reallly.
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
set background=dark

" Settings to enable fancy fonts for airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" File Explorer settings
let g:netrw_banner=0
let g:netrw_winsize=10

" Vimtex settings
let g:vimtex_quickfix_autoclose_after_keystrokes=3
let g:vimtex_quickfix_open_on_warning=0
let g:vimtex_view_general_viewer = 'SumatraPDF'
let g:vimtex_view_general_options = '-reuse-instance -forward-search @tex @line @pdf'
let g:vimtex_view_general_options_latexmk = '-reuse-instance'
let g:vimtex_compiler_latexmk_engines = {
            \ 'pdflatex'         : '-pdf',
            \ 'dvipdfex'         : '-pdfdvi',
            \ '_'         : '-lualatex',
            \ 'lualatex'         : '-lualatex',
            \ 'xelatex'          : '-xelatex',
            \ 'context (pdftex)' : '-pdf -pdflatex=texexec',
            \ 'context (luatex)' : '-pdf -pdflatex=context',
            \ 'context (xetex)'  : '-pdf -pdflatex=''texexec --xtx''',
            \}

" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Markdown preview plugin settings
let g:mkdp_auto_start = 1
let g:mdkp_browser = 'Mozilla Firefox'
let g:mdkp_filetypes = ['md', 'markdown', 'plantuml', 'uml']

" LanguageTool plugin settings
let g:languagetool_jar='$HOME/LanguageTool-5.4/languagetool-commandline.jar'

" Easymotion settings
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes
" `s{char}{char}{label}`
nmap <Leader>s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" Trigger autoread when changing buffers or coming back to vim.
au FocusGained,BufEnter * :silent! !

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" Fuzzy finding
set path+=**
nnoremap <C-p> :find ./**/*

" Switch between split windows
nnoremap <leader>h :wincmd h<CR>
" nnoremap <leader>j :wincmd j<CR>
" nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" Resize vertical split
nnoremap <silent> <Leader>+ :vertical resize +5<CR>
nnoremap <silent> <Leader>- :vertical resize -5<CR>

" Move visual selection up or down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Copy & Paste helpers (Amazing stuff!)
nnoremap <leader>y "+y
vnoremap <leader>y "+y
"nnoremap <leader>p "+p // TODO: I need to find a better key for that one or for the buffer navigation (see below)

" Delete without messing up with registers
nnoremap <leader>d "_d
vnoremap <leader>d "_d

" Switch between buffers
nnoremap <leader>p :bprevious<CR>
nnoremap <leader>P :bfirst<CR>
nnoremap <leader>n :bnext<CR>
nnoremap <leader>N :blast<CR>

" Open terminal below with
nnoremap <leader>c :bo term++rows=10<CR>

" Toggle relative line numbers
nnoremap <leader># :set rnu!<CR>

" Trim all trailing whitespace on saving buffer
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup somegroup
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
augroup end

" Automatically resize (I do not really know, what it's doing)
augroup ReduceNoise
    autocmd!
    autocmd WinEnter * :call ResizeSplits()
augroup END

function! ResizeSplits()
    set winwidth=90
    wincmd=
endfunction

"""" COC """"
"
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
else
    inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
            \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    elseif (coc#rpc#ready())
        call CocActionAsync('doHover')
    else
        execute '!' . &keywordprg . " " . expand('<cword>')
    endif
endfunction

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder.
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)
