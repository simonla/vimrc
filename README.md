**WARNING: This is Work in Progress!**

# Vimrc

My personal vim configuration.

## Usage

1. Clone the repo into your `.vim` folder:

    ```sh
    cd ~/.vim
    git clone https://gitlab.com/simonla/vimrc/
    ```
2. Create a symlink to the `.vimrc` in your user folder:
    - Unix:
        ```sh
        cd ~
        ln -s ~/.vim/vimrc/.vimrc ~/.vimrc
        ```
    - Windows:
        ```cmd
        cd ~
        mklink /H ~/.vim/vimrc/.vimrc ~/_vimrc
        ```
3. Install vim-plug as a Plugin manager: [see instructions](https://github.com/junegunn/vim-plug#installation)
